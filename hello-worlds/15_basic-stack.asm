Name : BasicStack

.model small

.stack 100h   ; the size of the stack

.data     

  
.code

main proc 

  pushf   ; puts the Flags register in the Stack
      
  mov ax, 7
  mov bx, 3
  mov cx, 5
  mov dx, 12

  push ax
  push cx
  push dx
  push bx 
  ; stack (from top -> bottom): 3, 12, 5, 7

  pop ax  ; 3
  pop bx  ; 12
  pop cx  ; 5
  pop dx  ; 7
  
  popf    ; puts the value SP points to in the Flags register


    main endp
end
