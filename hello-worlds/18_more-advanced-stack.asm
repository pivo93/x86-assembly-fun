Name : MoreAdvancedStack

.model small

.stack 100h   ; the size of the stack

.data


.code

main proc

pushf   ; puts the Flags register in the Stack

push 3
push 12
push 5
push 7
; stack (from top -> bottom): 7, 5, 12, 3

; Say we now want to modify the `12` on the stack. In order to do this, we would have to pop 3 values and push them onto the stack again.
; Or, we use the subsequent more advanced approach:
mov bp, sp          ; move the stack pointer into the base pointer (the SP currently points at the `7`)
mov dx, [bp + 4]    ; store the number `12` into DX (note that each value takes 2 memory rows, i.e. `bp + 4` and not `+2`)
inc dx              ; do something with the number, e.g. incrementing it
mov [bp + 4], dx    ; store the new value in the stack at the previous position (where `12` was)

popf    ; puts the value SP points to in the Flags register

main endp
end
