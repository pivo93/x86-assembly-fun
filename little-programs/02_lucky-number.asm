Name : LuckyNumber

;;;
; This program take a number from the user and prints "You are lucky" if the number is a 6.
;;;

.model small

.data 

  msg1 db 'Enter a number between 0 and 9', 10, 13, '$'
  msg2 db 10, 13, 'You have entered the number: $'
  msgLucky db 10, 13, 'You are lucky!!!$'
  msgUnlucky db 10, 13, 'Better luck next time...$'
  msgError db 10, 13, 'You did not enter a number!!!$'

  userNum db ?
  
.code

main proc   

    mov ax, @data
    mov ds, ax 
    
    ; print message to user
    mov ah, 9
    lea dx, msg1 
    int 21h

    ; read a char from the user
    mov ah, 1      
    int 21h 
    mov userNum, al

    ; verify that it is a number (i.e. 48 >= userChar <= 57)
    cmp userNum, 48
    jb noNumber
    cmp userNum, 57
    ja noNumber
    jna else

    noNumber:
        ; print error message to the user
        mov ah, 9
        lea dx, msgError
        int 21h

        jmp leaveDOS  

    else:
        ; print message to the user: "You have entered the number: "
        mov ah, 9
        lea dx, msg2
        int 21h  
    
        ; print the previously entered number to the user
        mov ah, 2
        lea dx, userNum
        int 21h           
        
        ; check if the `userNum` is our lucky number 6
        cmp userNum, '6'
        je lucky
        jne unlucky
    
        unlucky:
            ; print unlucky msg to the user
            mov ah, 9
            lea dx, msgUnlucky
            int 21h
            
            jmp leaveDOS
            
        lucky:
            ; print lucky msg to the user
            mov ah, 9
            lea dx, msgLucky
            int 21h
            
            jmp leaveDOS        
        
    leaveDOS:
        ; leaves the DOS and returns control back to the OS       
        mov ah, 4ch
        int 21h
        ret

    main endp  
end
