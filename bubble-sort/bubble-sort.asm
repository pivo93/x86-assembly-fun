Name : BubbleSort

;;;
; This program sorts an array of numbers using bubble sort.
;;;

.model small

.data
 
  numbers db 5,4,2,9,8,1,12,7,12,5,13,6,1,0,12  ; -> 0, 1, 1, 2, 4, 5, 5, 6, 7, 8, 9, 12, 12, 12, 13 

    ; gets the number of elements in `numbers` dynamically
    numbersLen=($-numbers)

  outerCX dw ?
  alreadyProcessedElements dw 0

.code

main proc
    
    mov ax,@data
    mov ds,ax

    ; we prepare `numbers` for the for-loop
    mov cx, numbersLen   
    dec cx                   ; we don't have to iterate over the last element as it would already be in place when we are at the 2nd last
    mov si, 0                ; we want to start a the first element, i.e. at `numbers[0]`          

    for:
      ; we need to pre-store the current value of CX of the outer for-loop
      mov outerCX, cx

      ; we  now prepare `numbers` for the inner for-loop
      mov cx, numbersLen  
      dec cx                           ; we don't have to iterate over the last element as it would already be in place when we are at the 2nd last
      sub cx, alreadyProcessedElements ; we want to start at the current element of the outer loop and not process already processed elements  
      mov di, 0     

      forInner: 
        mov al, numbers[di]
        mov bl, numbers[di + 1]
        cmp al, bl
        ; `swap` if `numbers[di] > numbers[di + 1]`
        je continueInner  
        jb continueInner
    
        swap:
          ; numbers[di] = numbers[di + 1] && numbers[di + 1] = numbers[di]
          mov al, numbers[di]
          mov bl, numbers[di + 1]
          
          mov numbers[di], bl
          mov numbers[di + 1], al
    
        continueInner:
          inc di
          loop forInner
      
      ; restore the outer for-loop's CX value
      mov cx, outerCX

      inc si
      inc alreadyProcessedElements
      loop for

  main endp

end
