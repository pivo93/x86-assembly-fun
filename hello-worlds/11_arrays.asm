Name : Arrays

.model small

.data

    ; here, "db" means that each of the elements (grades) has a size of 1 byte
    studentGrades db 1,2,5,5,4,2,9

    ; here, we have "dw", i.e. each element has a size of 2 bytes
    stockValues dw 1500, 2000, 3000

    ; here, we initialze an array with 10 elements of size "db" (1 byte), with the initial value being 5 for each element
    blub db 10 dup(5)

    ; variable to hold the sum of all grades in `studentGrades`
    sum db ?

.code

main proc   

    mov ax, @data
    mov ds, ax 
    
    ; store the student's 1st grade (i.e., offset+0) in register AL
    mov al, studentGrades

    ; store the student's 3rd grade (i.e., offset+2) in register BL 
    mov bl, studentGrades+2
    mov bh, [studentGrades+2] ; same as: `mov bl, studentGrades+2`

    ; since `stockValues` are of a size of 2 bytes per element, we must use `offset+4` to access the 3nd element
    ;       stockValues[0] -> offset+0, stockValues[1] -> offset+2, stockValues[2] -> offset+4
    mov cx, stockValues+4 
    
    ; test that `blub` works as expected
    mov dl, blub+5


    ; For-loop to get the sum of all elements in array `studentGrades`
    mov cx, 7       ; element count of `studentGrades
    mov dl, 0       ; temporary sum -> DL will in the end hold our sum 

    ; store the start address of `studentGrades` into register SI -> The purpose of this register is exactly to access different array elements
    lea si, studentGrades

    for: 
        add dl, [si]    ; add the value of `studentGrades[si]` to our sum in DL
        inc si          ; move the pointer to the next element of `studentGrades`
        loop for        ; decreases the value in CX until it's 0

        mov sum, dl

    main endp  
end
