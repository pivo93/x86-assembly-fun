Name : ReadFromKeyboadAndPrint

.model small

.data 

  char1 db '?'
  
.code

main proc   
              
    ; read character from keyboad (it is stored in register `al`) 
    mov ah, 1      
    int 21h      
             
    ; print character
    mov ah, 2    
    mov dl, al      ; move the previously read character into `dl`, the register whose value will be printed
    int 21h
           
    ; leaves the DOS and returns control back to the OS       
    mov ah, 4ch
    int 21h

    main endp  
end
