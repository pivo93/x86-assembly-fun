Name : NumberLoops

;;;
; This program expects from the user a number between 0 and 9 and will keep asking the user for a number until this condition is met.
;;;

.model small

.data 

  msg1 db 'Enter a number between 0 and 9', 10, 13, '$'
  msg2 db 10, 13, 'Nice number!$'
  msgError db 10, 13, 'You did not enter a number between 0 and 9!', 10, 13, '$'

  userNum db ?
  
.code

main proc   

    mov ax, @data
    mov ds, ax 
    
    ; the below is the equivalent of a `do-while` loop that gets executed at least once
    repeat:
        ; print message to user
        mov ah, 9
        lea dx, msg1 
        int 21h

        ; read a char from the user
        mov ah, 1      
        int 21h 
        mov userNum, al

        ; verify that it is a number (i.e. 48 >= userChar <= 57)
        cmp userNum, 48
        jb noNumber
        cmp userNum, 57
        ja noNumber
        jna continue

    noNumber:
        ; print error message to the user
        mov ah, 9
        lea dx, msgError
        int 21h

        ; jump back to the beginning of the loop
        jmp repeat  

    continue:
        ; print message to the user
        mov ah, 9
        lea dx, msg2
        int 21h  
           
        jmp leaveDOS        

    leaveDOS:
        ; leaves the DOS and returns control back to the OS       
        mov ah, 4ch
        int 21h
        ret

    main endp  
end
