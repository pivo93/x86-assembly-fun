# x86 Assembly Fun

This project contains a few fun most-basic x86 assembly programs to run on the EMU8086 Microprocessor Emulator.

Special thanks to A. Simohamed and H. E. Bouhanik and their "Complete x86 Assembly Programming" course on Udemy. All code here is directly inspired from participation in this course.

# Goal

The final goal of this endeavour was implementing the bubble sort algorithm in assembly
