Name : SimpleMathsFun

.model small

.data     

  var1 db 5
  var2 db 7
  var3 db ?
  var4 db ?  
  
.code

main proc 
      
    ; we need this when our program deals with variables 
    mov ax, @data
    mov ds, ax
            
    ; we want to calculate: var3 = var1 + var2
    mov al, var1      
    mov bl, var2
    add al, bl      ; -> al = al + bl
    mov var3, al    ; var3 = 12          
    
    ; now, we calculate: var1 = var2 - 5
    mov bl, var2
    sub bl, 5
    mov var1, bl   
                                    
    ; `inc op` -> Adds 1 to `op` 
    inc var1   
    
    ; `dec op` -> subtracts 1 from `op`
    dec var1 
    
    ; `neg op` -> negates the `op`
    neg var1 
    
    ; Integer Overflow (nice!) -> `Carry Flag` (CF) becomes 1 -> CF takes care of marking unsigned overflows 
    mov al, 255
    add al, 2       ; register `al` is now technically 257, but since it is only 8 bits (i.e. the max value is 255 for unsigned), it wraps around and is 1
     
    ; Integer Underflow -> `Overflow Flag` (OF) becomes 1 -> OF takes care of marking signed overflows 
    mov bl, -128
    add bl, -2      ; register `bl` is now technically -130, but since it is only 8 bits (i.e. the in value is -128 for signed), it wraps around and is 126
         
    main endp
end
