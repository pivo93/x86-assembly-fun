Name : ReadAndConvertToLowercaseAndPrint

;;;
; This program takes an uppercase character from the user keyboard, and validates that it actually is an uppercase character.
; It then converts it to a lowercase character and prints it back to the DOS terminal.
;;;

.model small

.data 

  message1 db 'Hello!', 10, 13, 'Please provide an uppercase letter which will be converted to lowercase: $' ; `10, 13` adds a new line
  message2 db 10, 13, 'The uppercase version of your char is: $'
  messageError db 10, 13, 'This was not an uppercase character!!!$'

  userChar db ?
  
.code

main proc   

    ; we save the address of the data segment in the register AX (so that it is available in the main memory) and then store 
    ; it into register DS, which denotes the "Data Segment". This allows us to deal with the variables in the program safely
    mov ax, @data
    mov ds, ax 
    
    ; use function code 9 to print some message to the user
    mov ah, 9
    lea dx, message1     ; puts the address of variable `message1` in the DX register
    int 21h

    ; read an uppercase char
    mov ah, 1      
    int 21h 
    mov userChar, al

    ; verify that it is an uppercase char (i.e. 65 >= userChar <= 90)
    cmp userChar, 65
    jb noUppercase
    cmp userChar, 90
    ja noUppercase

    ; convert char to lowercase
    add userChar, 32

    ; print message to the user
    mov ah, 9
    lea dx, message2
    int 21h

    ; print uppercase version of the char
    mov ah, 2
    mov dl, userChar 
    int 21h
           
    leaveDOS:
        ; leaves the DOS and returns control back to the OS       
        mov ah, 4ch
        int 21h
        ret

    noUppercase:
        ; use function code 9 to print something to the user
        mov ah, 9
        lea dx, messageError
        int 21h

        jmp leaveDOS

    main endp  
end
