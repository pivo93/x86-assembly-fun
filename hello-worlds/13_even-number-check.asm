Name : EvenNumber

.model small

.data    

    num db 11010011b
  
.code

main proc   
    
    mov al, num
    
    and al, 00000001b 
    
       ; if the last bit of `num` is 0, it is even. Otherwise, it is odd.   
       ; This means that AL will hold 1 if the number is odd and 0 if the number is even
    
    
    main endp  
end
