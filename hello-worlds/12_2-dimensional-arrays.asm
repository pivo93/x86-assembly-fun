Name : Arrays

.model small

.data

    ; initialize a 2-dimensional array
    array db 12, 15, 8, ?
          db 2, 8, 3, 16
          db 1, 2, 3, 4

          arrLen=($-array) ; =12

    array2 db 4 dup(5)
           db 4 dup(9)
           dw 4 dup(12)
           
           arr2Len=($-arra2y) ; =16 -> NOTE that the last row is "dw"

.code

main proc   

    mov ax, @data
    mov ds, ax 

    ; do something with the arrays...
    

    main endp  
end
