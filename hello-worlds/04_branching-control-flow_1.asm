Name : BranchingControlFlow

.model small

.data  

  num1 db 5
  num2 db 6   
  
.code

main proc   
    
    mov ax, @data
    mov ds, ax
       
    mov al, 5
    mov bl, 5
    sub al, bl
       
    ; a conditional jump to `someLabel` when the "Zero Flag" (ZF) is equal to 1 -> ZF is 1 when the previous instruction, `sub al, bl`,  results in 0
    jz someLabel 

    ; ^ the above is basically the equivalent of: `if (al - bl == 0) { jump to someLabel }`
    
    mov cl, 12  ; skipped because of the jump
    add cl, bl  ; skipped
      
    someLabel: inc al
    
    ; we can use `jnz` to jump when the ZF is NOT raised
    jnz someLabel2
    
    mov dl, 15 ; skipped because the previous `inc al` will not result in 0
    
    someLabel2: inc dl  

    ; jump only if Integer Overflow was detected, i.e. if the CF is 1              
    mov al, 255 ; unsigned
    add al, 1
    jc overflow
    
    mov al, 5; skipped
    
    overflow: mov al, 255 ; reset the previous value (why not?)      
     
     
 
    ; next, we compare numbers and jump based on the result
    mov al, num1
    sub al, num2 
    
    ; jumps if num2 > num1 ; `al` now has a negative value and the SF ("Signed Flag") is raised to 1
    js num2Greater
    
    ; jumps if num2 == num1 ; `al` now has the value 0 and the ZF ("Zero Flag") is raised to 1 
    jz num2Equal  
        
    mov bl, 123     ; only not skipped if num1 >= num2
    
    num2Greater: 
        mov bx, 456     
        jmp continue
    
    num2Equal: 
        mov dx, 789
        jmp continue

    continue:
        ; Alternative way to check if num2 > num1 for unsigned numbers
        mov al, num1
        mov bl, num2
        cmp bl, al
    
        ; jumps if num2 > num1, i.e. if the first value to compare is "above" the other
        ja num2GreaterAgain     ; `ja` = "jump above" --> This is only used for unsigned numbers.. 
                                ; -> for signed numbers, we would use `jg` -> "jump if greater"
        
        ; we can also jump without any condition via `jmp <label>`
        jmp theEnd 
        
        num2GreaterAgain:
            jmp theEnd
    
        theEnd: ret
        
    main endp  
end
