Name : ForLoop

.model small

.data 
  
.code

main proc   
    
    mov ah, 2
    mov dl, 'a'
    mov cx, 26 ; the alphabet has 26 letters

    ; print the alphabet
    for:
        int 21h
        inc dl
        loop for ; automatically decreases the value in register CX by 1. Breaks out of the loop if CX is 0

    ; leaves the DOS and returns control back to the OS       
    mov ah, 4ch
    int 21h
    ret

    main endp  
end
