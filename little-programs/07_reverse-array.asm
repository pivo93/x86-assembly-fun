Name : ReverseArray

;;;
; This program reverse the values in an array.
;;;

.model small

.stack 100h

.data 

  numbers dw 5,4,2,9,10,5,4,3,12

    ; gets the lenght of `numbers` dynamically
    numLen=($-numbers)
   
  amountOfNum dw ?
   
.code

main proc   

    mov ax, @data
    mov ds, ax  
    
    ; since we have an array of "dw", we need to divide `numLen` by 2 to have the actual amount of numbers
    mov ax, numLen
    shr ax, 1
    mov amountOfNum, ax

    ; we prepare `numbers` for the for-loop 
    mov cx, amountOfNum
    mov si, 0       ; we start at `numbers[0]`

    ; push all numbers onto the stack
    for1:
      push numbers[si]
      add si, 2         ; we need to iterate through `numbers` in steps of 2's since it's "dw" (and not "db")
      loop for1

    ; we prepare `numbers` for the for-loop
    mov cx, amountOfNum
    mov si, 0        ; we start at `numbers[0]`

    ; simply pop them off again in reverse order
    for2:
      pop numbers[si]
      add si, 2
      loop for2

    main endp  
end
