Name : BranchingControlFlow2

.model small

.data  
  
.code

main proc   

    mov al, 5
    mov bl, -2

    cmp al, bl      
                
    ; (jump greater) -> jump if al > bl
    jg alGreater
    
    ; (jump lower) -> jump if al < bl
    jl alLower
    
    ; (jump equal) -> jump if al = bl
    je alEqual

    alGreater:
        mov dl, 0
        ret

    alLower:
        mov dl, 1
        ret
        
    alEqual:
        mov dl, 2
        ret

    main endp  
end
