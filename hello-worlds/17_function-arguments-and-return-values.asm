Name : FunctionArgumentsAndReturnValues

.model small

.stack 100h   ; the size of the stack

.data     

.code

main proc 

    ; next, we use function parameters and return values so that we can use the procedure in any program.
    ; For this, we use the stack
    mov ax, 1
    mov bx, 4

    ; thus, we push the 2 function parameters for `addition` onto the stack before calling it
    push ax
    push bx
    call addition   ; we want to calculate `1 + 4`
    ; the return value of `addition` is now the first element on the stack
    pop dx      ; holds 5 now
    
    hlt
    main endp

addition proc

    ; first, we need to pop the procedure-linking information that was pushed onto the stack by the `call` instruction
    pop ax
 
    ; we pop the last 2 numbers that were pushed onto the stack
    pop cx
    pop dx

    ; now we perform the purpose of this procedure
    add cx, dx

    ; now we return the result of the addition via the stack
    push cx   
           
    ; last, we restore the top of the stack again. This is very important because without it, our `ret` jumps to the wrong instruction,
    ; which is because of the `IP` having a wrong value
    push ax

    ret
    addition endp

end
