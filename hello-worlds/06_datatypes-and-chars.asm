Name : DatatypesAndChars

.model small

.data 

  char1 db 'A' 
  char2 db '2'
  char3 db ? 
  
.code

main proc   

    mov al, 'B'    
    mov char1, al
    
    mov bl, char1

    ; 'B' + 1 -> the ASCII equivalent of 'C'
    add bl, 1
    mov char3, bl   ; char3 = 'C'
    
    mov cl, char3

    ; convert char from uppercase to lowercase
    mov dl, 'X'
    add dl, 32

    main endp  
end
