Name : FirstProgram

.model small

.data     

; Generally: we initialize a variable by reserving some space in memory. Variables are generally used when the 4 data registers are not enoughs
; i.e. a variable = some space into memory with a defined size and start address
      
 ; constants: 
 Pi EQU 3.1428   
 C  EQU 15
      
 ; we define a variable "someVar", allocate some space (db = "define bytes" = 1 byte = 1 memory row), and provide an initial value (e.g. 12)                                                                                                       
 someVar db 12
 someOtherVar db ?   
 
 ; data types are:
 ;  - "db" (define byte) 
 ;  - "dw" (define word -> 2 byes = 2 consecutive memory rows)
 ;  - "dd" (define double -> 4 bytes)
 ;  - "dq" (define quadword -> 8 bytes) 
 ;  - "dt" (define ten -> 10 bytes)
 var1 db 15         ; -128 <= db <= 255
 var2 dw -151        ; -32768 <= dw <= 65536
 var3 dd 115
 ; var4 dq 6
 ; var5 dt 5  
 
 ; define a binary number (end with "b")
 varBinary db 1001B ; can have a maximum of 8 bites (because it is a "db"), i.e. max 8 binary digits 
 
 ; define a hexadecimal number (ends with "h") -> Note that this must start with a "0" if the first digit is a letter 
 varHexadecimal db 0faH
  
.code

main proc 
    ; mov dest, src ; takes value from the `src` and moves it to the `dest` (note that `src` and `dest` should both have the same size and cannot both be variables)
    mov al, 15    
    mov dh, al   
    
    mov var1, 16  
    
    main endp
end


