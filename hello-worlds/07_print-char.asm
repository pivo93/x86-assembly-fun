Name : PrintToDOS

.model small

.data 

  char1 db ?
  
.code

main proc   
              
    ; Print character 'T' to DOS --> NOTE: This only works in DOS, not in modern Windows versions
   
    mov ah, 2       ; set the `ah` register to match with the `02` function code (write to STDOUT)
    mov dl, 'T'     ; the character to print
    int 21h         ; trigger the 21h interrupt

    main endp  
end
