Name : MoreComplexMaths

.model small

.data     

  num1 db 3
  num2 db 1010b
  num3 db 8
  num4 dw -12
  
.code

main proc 
      
    ; we need this when our program deals with variables 
    mov ax, @data
    mov ds, ax
            
    ; Basic Shift-left (basically adds a 0 to the very right and shifts all left)
    mov cl, num2    ; 1010b := 10 decimal
    shl cl, 1       ; 1010b << 1 = 10100b := 20 decimal 

    ; Basic Shift-right (basically adds a 0 to the very left and shifts all right, i.e. the rightest bit "drops off")
    mov ch, num2    ; 1010b := 10 decimal
    shr ch, 1       ; 1010b >> 1 = 0101b := 5 decimal 

    ; Multiplication via Shift-left:
    
    ; shift-left once means multiplying by 2
    mov cl, num1
    shl cl, 1     ; 3 << 1 = 3 * (2 ^ 1) = 6
    
    ; shift-left twice means multiplying by 4
    mov dl, num1
    shl dl, 2     ; 3 << 2 = 3 * (2 ^ 2) = 12  

    ; shift-left 3 times means multiplying by 8
    mov al, num1
    shl al, 3     ; 3 << 3 = 3 * (2 ^ 3) = 24 


    ; Division via Shift-right (this only works for positive numbers!! -> We use `sar` for negative numbers):
    ; shift-right once means dividing by 2
    mov ch, num3 
    shr ch, 1     ; 8 >> 1 = 8 / (2 ^ 1) = 4

    ; shift-right twice means dividing by 4
    mov dh, num3 
    shr dh, 2     ; 8 >> 2 = 8 / (2 ^ 2) = 2

    ; shift-right 3 times means dividing by 8
    mov ah, num3 
    shr ah, 3     ; 8 >> 3 = 8 / (2 ^ 3) = 1

    ; Division of negative numbers via `sar`
    mov ax, num4
    sar ax, 1     ; -12 >> 1 = -6

    mov bx, num4
    sar bx, 2     ; -12 >> 2 = -3


    ; Multiplication via `mul` and `imul` (for signed)
    mov ax, 3
    mov bx, 10
    mul bx       ; `mul` always multiplies AX by a value (e.g. BX) and stores the result in AL

    mov ax, -5
    mov dx, 10
    imul dx

    ; Division via `div` and `idiv` (for signed)
    mov ax, 25
    mov bl, 5
    div bl       ; `div` always divides AX by a value (e.g. BX) and stores the result in AL (and a potential modulo remainder in AH)

    mov ax, -50
    mov dl, 5
    idiv dl

    main endp
end
