Name : GetMinValueOfArray

;;;
; This program gets the min value of an array of numbers.
;;;

.model small

.data 

  numbers db 5,4,2,9,8,1,12

    ; gets the number of elements in `numbers` dynamically
    numbersLen=($-numbers)

  minVal db ?
  
.code

main proc   

    mov ax, @data
    mov ds, ax 

    ; we initialize `minVal` with the first element of `numbers`
    mov al, numbers
    mov minVal, al

    ; we prepare `numbers` for the for-loop
    lea si, numbers
    mov cx, numbersLen

    for:
        ; check if `minVal` is "above" our current element, which would mean that `<currentElement>` is smaller than `minVal` 
        ; and, hence, would make it our new `minVal`
        mov dl, minVal  
        cmp dl, [si]

        ja newMinVal
        je continue
        jb continue

        newMinVal:
            ; now we store  `<currentElement>` into `minVal` and continue our loop
            mov dl, [si]
            mov minVal, dl

        continue:
            ; now we get to the next element of `numbers`
            inc si

            ; decrease the value in CX and jump to the beginning of the loop
            loop for

    ; ultimately, we store `minVal` into AL
    mov al, minVal

    main endp  
end
