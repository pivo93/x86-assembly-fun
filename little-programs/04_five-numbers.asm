Name : FiveNumbers

;;;
; This program expects from the user fives numbers and prints them back.
;;;

.model small

.data 

  userNumbers db 5 dup(?)

  msg1 db 'Enter 5 numbers', 10, 13, '$'
  msg2 db 10, 13, 'You have entered: ', 10, 13, '$'
  
.code

main proc   

    mov ax, @data
    mov ds, ax 

    ; print message to the user
    mov ah, 9
    lea dx, msg1
    int 21h

    ; prepare for loop to be executed 5 times
    mov cx, 5
    lea si, userNumbers

    for1:
        ; read number from the user
        mov ah, 1
        int 21h

        ; store the read number into the address of the current element of `userNumbers`, depicted by `[si]`
        mov [si], al

        ; now we get to the next element of `userNumbers`
        inc si

        ; carriage return
        mov ah, 2
        mov dl, 10
        int 21h
        mov dl, 13
        int 21h

        ; decrease the value in CX and jump to the beginning of the loop
        loop for1

    ; print message to the user
    mov ah, 9
    lea dx, msg2
    int 21h

    ; prepare for loop to be executed 5 times
    mov cx, 5
    lea si, userNumbers

    for2: 
        ; print the number to the user
        mov ah, 2
        mov dl, [si]    ; stores the current element's value of `userNumbers` into DL
        int 21h

        ; now we get to the next element of `userNumbers`
        inc si

        ; carriage return
        mov ah, 2
        mov dl, 10
        int 21h
        mov dl, 13
        int 21h

        ; decrease the value in CX and jump to the beginning of the loop
        loop for2   
     
    leaveDOS:
        ; leaves the DOS and returns control back to the OS       
        mov ah, 4ch
        int 21h
        ret

    main endp  
end
