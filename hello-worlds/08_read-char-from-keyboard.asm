Name : ReadFromKeyboad

.model small

.data 

  char1 db '?'
  
.code

main proc   
              
    ; Read character from DOS keyboad (it is stored in register `al`)
   
    mov ah, 1       ; set the `ah` register to match with the `01` function code (read to STDIN)
    int 21h         ; trigger the 21h interrupt

    main endp  
end
