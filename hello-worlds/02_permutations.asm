Name : Permutations

.model small

.data     

  C EQU 15
  var1 db 12
  var2 db ?  
  var3 db 9
  var4 db 10
  
.code

main proc   
    ; we need this when our program deals with variables 
    mov ax, @data
    mov ds, ax
    
    mov var2, C    
            
    ; next: var1=var2 and var2=var1 (permutation)
    mov al, var2 ; we first store var2 into a register
    mov bl, var1
    mov var1, al  
    mov var2, bl  
    
    ; another way to do the permutation (e.g. var3=var4, var4=var3) is the following:
    mov al, var3
    mov bl, var4
    xchg al, bl      ; `xchg op1 op2` -> the value of `op1` will be in `op2` and vice versa    
    mov var3, al
    mov var4, bl   
         
    main endp
end
