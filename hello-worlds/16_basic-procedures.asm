Name : BasicProcedures

.model small

.stack 100h   ; the size of the stack

.data     

  
.code

main proc 

    mov ax, 1 

    call proc1 

    mov bx, 2 
    
    call proc2

    mov dx, 4 
    ret

    main endp

proc1 proc
    add ax, 1

    ret 
    proc1 endp
  
proc2 proc
    add bx, 3
    
    ret
    proc2 endp

end
