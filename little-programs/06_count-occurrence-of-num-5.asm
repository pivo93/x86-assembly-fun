Name : CountOccurrencesOfNum5

;;;
; This program counts the occurrences of the number 5 in an array.
;;;

.model small

.data 

  numbers db 5,4,2,9,8,1,12,5,5,4

    ; gets the number of elements in `numbers` dynamically
    numbersLen=($-numbers)

  amountOfFives db ?
  
.code

main proc   

    mov ax, @data
    mov ds, ax 

    ; we initialize `amountOfFives` with 0
    mov amountOfFives, 0

    ; we prepare `numbers` for the for-loop
    mov cx, numbersLen
    mov si, 0               ; NOTE: This time, we use a different way than before to access an array

    for:
        cmp numbers[si], 5
        jne continue

        equal:
            inc amountOfFives
           
        continue:
            ; now we get to the next element of `numbers`
            inc si

            ; decrease the value in CX and jump to the beginning of the loop
            loop for    
 
    ; ultimately, we store `amountOfFives` into AL
    mov al, amountOfFives

    main endp  
end
